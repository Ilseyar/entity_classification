# -*- coding: utf-8 -*-

from layers.dynamic_rnn import DynamicLSTM
from layers.attention import Attention
import torch
import torch.nn as nn


class LSTM_CA(nn.Module):
    def __init__(self, embedding_matrix, opt):
        super(LSTM_CA, self).__init__()
        self.opt = opt
        self.embed = nn.Embedding.from_pretrained(torch.tensor(embedding_matrix, dtype=torch.float), freeze=True)
        self.lstm_context = DynamicLSTM(opt.embed_dim, opt.hidden_dim, num_layers=1, batch_first=True)
        self.lstm_entity = DynamicLSTM(opt.embed_dim, opt.hidden_dim, num_layers=1, batch_first=True)
        self.attention_entity = Attention(opt.hidden_dim, score_function='bi_linear')
        self.attention_context = Attention(opt.hidden_dim, score_function='bi_linear')
        self.dense = nn.Linear(opt.hidden_dim*2, opt.polarities_dim)

    def forward(self, inputs):
        text_raw_indices, entity_indices = inputs[0], inputs[1]
        text_raw_len = torch.sum(text_raw_indices != 0, dim=-1)
        entity_len = torch.sum(entity_indices != 0, dim=-1)

        context = self.embed(text_raw_indices)
        entity = self.embed(entity_indices)
        context, (_, _) = self.lstm_context(context, text_raw_len)
        entity, (_, _) = self.lstm_entity(entity, entity_len)

        entity_len = torch.tensor(entity_len, dtype=torch.float).to(self.opt.device)
        entity_pool = torch.sum(entity, dim=1)
        entity_pool = torch.div(entity_pool, entity_len.view(entity_len.size(0), 1))

        text_raw_len = torch.tensor(text_raw_len, dtype=torch.float).to(self.opt.device)
        context_pool = torch.sum(context, dim=1)
        context_pool = torch.div(context_pool, text_raw_len.view(text_raw_len.size(0), 1))

        entity_len = torch.tensor(entity_len, dtype=torch.float).to(self.opt.device)
        entity = torch.sum(entity, dim=1)
        entity = torch.div(entity, entity_len.view(entity_len.size(0), 1))

        text_raw_len = torch.tensor(text_raw_len, dtype=torch.float).to(self.opt.device)
        context = torch.sum(context, dim=1)
        context = torch.div(context, text_raw_len.view(text_raw_len.size(0), 1))

        entity_final = self.attention_entity(entity, context_pool).squeeze(dim=1)
        context_final = self.attention_context(context, entity_pool).squeeze(dim=1)

        x = torch.cat((entity_final, context_final), dim=-1)
        out = self.dense(x)
        return out