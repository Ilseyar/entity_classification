import nltk
import numpy
from scipy import sparse
from sklearn import preprocessing

from sklearn.base import BaseEstimator

class SentimentFeature(BaseEstimator):

    # window_words = []

    def __init__(self):
        pass

    def get_feature_names(self):
        return 'sentiment'

    def fit(self, documents, y=None):
        return self

    def load_sentiment_dict_subj(self):
        f = open("resources/sentiment/subjclueslen1-HLTEMNLP05.tff")
        sentiment_dict = {}
        for line in f:
            terms = line.split(" ")
            sentiment_dict[terms[2][terms[2].index("=") + 1:]] = terms[len(terms) - 1][
                                                                 terms[len(terms) - 1].index("=") + 1:].strip()
        return sentiment_dict

    def load_sem_eval_semantic(self):
        dict_sent = {}
        f = open("resources/sentiment/SemEval2015-English-Twitter-Lexicon.txt")
        for line in f:
            line_parts = line.split("\t")
            score = line_parts[0]
            if score > 0:
                pos_score = score
                neg_score = 0
            else:
                pos_score = 0
                neg_score = abs(score)
            dict_sent[line_parts[1].strip()] = {
                'pos_score': pos_score,
                'neg_score': neg_score
            }
        return dict_sent

    def load_sentiment_dict_word_net(self):
        f = open("resources/sentiment/SentiWordNet_3.0.txt")
        result = {}
        for line in f:
            if not (line.startswith("#") or line.startswith(";")):
                line_parts = line.split("\t")
                pos_score = line_parts[2]
                neg_score = line_parts[3]
                syn_terms_split = line_parts[4].split(" ")
                for syn_term_split in syn_terms_split:
                    result[syn_term_split.split("#")[0]] = {
                        'pos_score' : float(pos_score),
                        'neg_score' : abs(float(neg_score))
                    }
                    if syn_term_split.find('weird') != -1:
                        print(syn_terms_split)
        return result

    def create_sentiment_feature_word_net(self, sentiment_dict, window_word, negative_words, punctuation):
        feature = [0] * 8
        zero_pmi_score = 0
        total_score = 0
        max_score = 0
        last_score = 0
        zero_pmi_score_neg = 0
        total_score_neg = 0
        max_score_neg = 0
        last_score_neg = 0
        is_context_negated = False
        for word in window_word:
            if word in negative_words:
                is_context_negated = True
            elif word in punctuation:
                is_context_negated = False
            if word in sentiment_dict:
                sentiment = sentiment_dict[word]
                pos = float(sentiment['pos_score'])
                neg = float(sentiment['neg_score'])
                score = pos - neg
                if is_context_negated:
                    if score != 0:
                        zero_pmi_score_neg += 1
                    total_score_neg += score
                    if score != 0 and (score > max_score_neg or (abs(score) > max_score_neg and max_score_neg == 0)):
                        max_score_neg = score
                    last_score_neg = score
                else:
                    if score != 0:
                        zero_pmi_score += 1
                        if score > max_score or ( abs(score) > max_score and max_score == 0):
                            max_score = score
                        last_score = score
                    total_score += score
        feature[0] = zero_pmi_score
        feature[1] = total_score
        feature[2] = max_score
        feature[3] = last_score
        feature[4] = zero_pmi_score_neg
        feature[5] = total_score_neg
        feature[6] = max_score_neg
        feature[7] = last_score_neg
        return feature

    def load_negated_words(self):
        f = open("resources/sentiment/negated_words.txt")
        negative_words_dict = []
        for line in f:
            negative_words_dict.append(line.strip())
        return negative_words_dict

    def create_sentiment_feature_subj(self, sentiment_dict, window_word, negative_words, punctuation):
        feature = [0] * 4
        positive_affirmative = 0
        negative_affirmative = 0
        positive_negated = 0
        negative_negated = 0
        is_negative_context = False
        for word in window_word:
            if word in negative_words:
                is_negative_context = True
            elif word in punctuation:
                is_negative_context = False
            if word in sentiment_dict:
                sentiment = sentiment_dict[word].strip()
                if sentiment == "negative":
                    if is_negative_context:
                        negative_negated += 1
                    else:
                        negative_affirmative += 1
                elif sentiment == "positive":
                    if is_negative_context:
                        positive_negated += 1
                    else:
                        positive_affirmative += 1
        feature[0] = positive_affirmative
        feature[1] = negative_affirmative
        feature[2] = positive_negated
        feature[3] = negative_negated
        return feature

    def load_bingliu_dict(self):
        bingliu_dict = {}
        f_negative = open("resources/sentiment/bingliunegs.txt")
        for line in f_negative:
            bingliu_dict[line.strip()] = "negative"
        f_positive = open("resources/sentiment/bingliuposs.txt")
        for line in f_positive:
            bingliu_dict[line.strip()] = "positive"
        return bingliu_dict

    def transform(self, window_words, y=None):
        features = []
        sentiment_bingliu_dict = self.load_bingliu_dict()
        for window_word in window_words:
            window_word_parts = nltk.word_tokenize(window_word)
            feature = [0] * 2
            for window_word in window_word_parts:
                if window_word in sentiment_bingliu_dict:
                    sentiment = sentiment_bingliu_dict[window_word]
                    if sentiment == 'negative':
                        feature[0] = 1
                    elif sentiment == 'positive':
                        feature[1] = 1
            features.append(feature)
        return features