from sklearn.base import BaseEstimator


class PMIFeature(BaseEstimator):

    def __init__(self):
        pass

    def get_feature_names(self):
        return 'pmi_feature'

    def fit(self, documents, y=None):
        return self

    def transform(self, window_words, y=None):
        f = open("resources/pmi.txt", 'r')
        word_dict = {}
        for line in f:
            line_parts = line.split("\t")
            word_dict[line_parts[0]] = float(line_parts[3])
        features = []
        max_len = 0
        for window_word in window_words:
            window_word_parts = window_word.split(" ")
            if len(window_word_parts) > max_len:
                max_len = len(window_word_parts)
        for window_word in window_words:
            window_word_parts = window_word.split(" ")
            feature = [0]
            for word in window_word_parts:
                if word in word_dict:
                    pmi = word_dict[word]
                    feature[0] += pmi
            features.append(feature)
        return features