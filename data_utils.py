# -*- coding: utf-8 -*-
# file: data_utils.py
# author: songyouwei <youwei0314@gmail.com>
# Copyright (C) 2018. All Rights Reserved.
import codecs
import json
import os
import pickle

import numpy
import numpy as np
import torch
from gensim.models import KeyedVectors
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.utils.validation import check_is_fitted
from torch.utils.data import Dataset

from features.brown_clusters import BrownClustersFeature
from features.extract_entities import ExtractEntities
from features.extract_window_words import ExtractWindowWords
from features.pos_tag_feature import PosTagFeatures
from features.sentiment_feature import SentimentFeature
from features.pmi_feature import PMIFeature
from features.umls_semantic_type_feature import UMLSemanticTypeFeature


def build_tokenizer(fnames, max_seq_len, dat_fname):
    text = ''
    for fname in fnames:
        fin = open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
        lines = fin.readlines()
        fin.close()
        for i in range(0, len(lines), 3):
            text_left, _, text_right = [s.lower().strip() for s in lines[i].partition("$T$")]
            entity = lines[i + 1].lower().strip()
            text_raw = text_left + " " + entity + " " + text_right
            text += text_raw + " "

    tokenizer = Tokenizer(max_seq_len)
    tokenizer.fit_on_text(text)
    pickle.dump(tokenizer, open(dat_fname, 'wb'))
    return tokenizer


def load_word2vec(path, word2idx = None):
    word_vec = {}
    w2v_model = KeyedVectors.load_word2vec_format(path, binary=True)
    in_vocb = 0
    words_in_voc = set()
    for word in w2v_model.vocab:
        if word2idx is None or word in word2idx.keys():
            word_vec[word] = w2v_model[word]
            in_vocb += 1
            words_in_voc.add(word)
    # print(set(word2idx.keys()).difference(words_in_voc))
    print("In vocabulary = %s, all words = %s", in_vocb, len(word2idx.keys()))
    return word_vec


def build_embedding_matrix(word2idx, embed_dim, embed_file, dat_fname):
    print('loading word vectors...')
    embedding_matrix = np.zeros((len(word2idx) + 2, embed_dim))  # idx 0 and len(word2idx)+1 are all-zeros
    fname = embed_file
    word_vec = load_word2vec(fname, word2idx=word2idx)
    print('building embedding_matrix:', dat_fname)
    for word, i in word2idx.items():
        vec = word_vec.get(word)
        if vec is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = vec
    pickle.dump(embedding_matrix, open(dat_fname, 'wb'))
    return embedding_matrix


def pad_and_truncate(sequence, maxlen, dtype='int64', padding='post', truncating='post', value=0):
    x = (np.ones(maxlen) * value).astype(dtype)
    if truncating == 'pre':
        trunc = sequence[-maxlen:]
    else:
        trunc = sequence[:maxlen]
    trunc = np.asarray(trunc, dtype=dtype)
    if padding == 'post':
        x[:len(trunc)] = trunc
    else:
        x[-len(trunc):] = trunc
    return x


class Tokenizer(object):
    def __init__(self, max_seq_len, lower=True):
        self.lower = lower
        self.max_seq_len = max_seq_len
        self.word2idx = {}
        self.idx2word = {}
        self.idx = 1

    def fit_on_text(self, text):
        if self.lower:
            text = text.lower()
        words = text.split()
        for word in words:
            if word not in self.word2idx:
                self.word2idx[word] = self.idx
                self.idx2word[self.idx] = word
                self.idx += 1

    def text_to_sequence(self, text, reverse=False, padding='post', truncating='post'):
        if self.lower:
            text = text.lower()
        words = text.split()
        unknownidx = len(self.word2idx)+1
        sequence = [self.word2idx[w] if w in self.word2idx else unknownidx for w in words]
        if len(sequence) == 0:
            sequence = [0]
        if reverse:
            sequence = sequence[::-1]
        return pad_and_truncate(sequence, self.max_seq_len, padding=padding, truncating=truncating)


class ABSADataset(Dataset):
    def __init__(self, fname, tokenizer):
        fin = open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
        lines = fin.readlines()
        fin.close()

        all_data = []
        for i in range(0, len(lines), 3):
            if i == 687:
                print(lines[i])
            text_left, _, text_right = [s.lower().strip() for s in lines[i].partition("$T$")]
            entity = lines[i + 1].lower().strip()
            polarity = lines[i + 2].strip()

            text_raw_indices = tokenizer.text_to_sequence(text_left + " " + entity + " " + text_right)
            text_raw_without_entity_indices = tokenizer.text_to_sequence(text_left + " " + text_right)
            text_left_indices = tokenizer.text_to_sequence(text_left)
            text_left_with_entity_indices = tokenizer.text_to_sequence(text_left + " " + entity)
            text_right_indices = tokenizer.text_to_sequence(text_right, reverse=True)
            text_right_with_entity_indices = tokenizer.text_to_sequence(" " + entity + " " + text_right, reverse=True)
            entity_indices = tokenizer.text_to_sequence(entity)
            left_context_len = np.sum(text_left_indices != 0)
            entity_len = np.sum(entity_indices != 0)
            entity_in_text = torch.tensor([left_context_len.item(), (left_context_len + entity_len - 1).item()])
            # polarity = int(polarity) + 1
            polarity = int(polarity)
            if entity_len != entity_in_text[1] - entity_in_text[0] + 1:
                print(lines[i] + "!!!!!!!!!!!!!!!!!!!!!!!!!")
                break

            text_bert_indices = tokenizer.text_to_sequence('[CLS] ' + text_left + " " + entity + " " + text_right + ' [SEP] ' + entity + " [SEP]")
            bert_segments_ids = np.asarray([0] * (np.sum(text_raw_indices != 0) + 2) + [1] * (entity_len + 1))
            bert_segments_ids = pad_and_truncate(bert_segments_ids, tokenizer.max_seq_len)

            text_raw_bert_indices = tokenizer.text_to_sequence("[CLS] " + text_left + " " + entity + " " + text_right + " [SEP]")
            entity_bert_indices = tokenizer.text_to_sequence("[CLS] " + entity + " [SEP]")

            data = {
                'text_bert_indices': text_bert_indices,
                'bert_segments_ids': bert_segments_ids,
                'text_raw_bert_indices': text_raw_bert_indices,
                'entity_bert_indices': entity_bert_indices,
                'text_raw_indices': text_raw_indices,
                'text_raw_without_entity_indices': text_raw_without_entity_indices,
                'text_left_indices': text_left_indices,
                'text_left_with_entity_indices': text_left_with_entity_indices,
                'text_right_indices': text_right_indices,
                'text_right_with_entity_indices': text_right_with_entity_indices,
                'entity_indices': entity_indices,
                'entity_in_text': entity_in_text,
                'polarity': polarity,
            }

            all_data.append(data)
        self.data = all_data

    def __getitem__(self, index):
        return self.data[index]

    def __len__(self):
        return len(self.data)


class ABSAFeaturesDataset(ABSADataset):

    brown_clusters_file = "resources/brown_clusters.txt"
    umls_semantic_types_file = "resources/umls_concept_id.txt"
    features_len = 0

    def load_brown_clusters(self):
        f = codecs.open(self.brown_clusters_file, 'r', 'utf_8_sig')
        dict_cluss = {}
        for line in f:
            terms = line.split("\t")
            dict_cluss[terms[1]] = terms[2].strip()
        return dict_cluss

    def load_umls_semantic_types(self):
        f = open(self.umls_semantic_types_file)
        dict_cluss = {}
        for line in f:
            line_parts = line.split("\t")
            dict_cluss[line_parts[0]] = line_parts[2].strip()
        return dict_cluss

    def __init__(self, fname, tokenizer, fname_features, vectorizer, transformer, is_train):
        super(ABSAFeaturesDataset, self).__init__(fname, tokenizer)
        all_data = self.data
        all_data_new = []
        f = open(fname_features)
        reviews = []
        for line, data in zip(f, all_data):
            reviews.append(json.loads(line))
        window_words = ExtractWindowWords().transform(reviews)
        entities_text = ExtractEntities().transform(reviews)

        if is_train:
            X = vectorizer.fit_transform(entities_text)
            print("train")
        else:
            X = vectorizer.transform(entities_text)
            print("test")
        X = X.toarray()

        pos_tag_feature = PosTagFeatures().transform(window_words)
        sentiment_feature = SentimentFeature().transform(window_words)
        brown_clusters = self.load_brown_clusters()
        umls_semantic_types = self.load_umls_semantic_types()
        brown_cls_feature = BrownClustersFeature(brown_clusters).transform(entities_text)
        umls_semantic_type_feature = UMLSemanticTypeFeature(umls_semantic_types).transform(entities_text)
        pmi_features = PMIFeature().transform(window_words)

        features = numpy.concatenate((X, pos_tag_feature), axis=1)
        features = numpy.concatenate((features, sentiment_feature), axis=1)
        features = numpy.concatenate((features, brown_cls_feature), axis=1)
        features = numpy.concatenate((features, umls_semantic_type_feature), axis=1)
        features = numpy.concatenate((features, pmi_features), axis=1)

        for feature, data in zip(features, self.data):
            data['features'] = feature
            all_data_new.append(data)

        self.data = all_data_new
        self.features_len = features.shape[1]
        print("Features shape = " + str(features.shape))
        print("Features length = " + str(self.features_len))

