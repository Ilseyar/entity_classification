## Description

This repository contains a source code of LSM_CA and LSTM_CA+features models for entity types classification.

The Cross Attention Long Short Term Memory (LSTM+CA) network consists of two parts, each of which creates a representation of the context and the entity using the vector representation of the words and the LSTM layer. The obtained vectors are averaged and used to calculate the attention vector. IAN uses attention mechanisms to detect the important words of the target entity and its full context. In the first layer of attention, the vector of context and the averaged vector of the entity and in the second, the vector of the entity and the averaged vector of context are applied. The resulting vectors are concatenated and transferred to the layer with the softmax activation function for classification. 

The LSTM+CA+feat model based on LSTM+CA neural network with additional features. The features: Bag of words, Part of Speech, Sentiments, Pointwise mutual information, Brown clusters, UMLS concept types. Features are concatenated with the representation learned by the neural network that captures extensional semantic information of an entity mention and the context. 

## Requirement

* PyTorch 0.4.0
* NumPy 1.13.3
* tensorboardX 1.2
* Python 3.6

## Model training

```angular2
python train.py
```

Required parameters
```angular2
--model_name // lstm_ca or lstm_ca_feat
--dataset // dataset name
--embed_file // file to pretrained word embedding model
--embed_dim // size of embedding vectors
```

### Add new corpus
1) Create new directory in datasets path, add train.txt, test.txt, train_features.txt and test_features.txt files
2) In train.py  add paths to train and test files in dataset_files dict
3) Count max context len, add to context_dict in train.py file