# -*- coding: utf-8 -*-
# file: train.py
# author: songyouwei <youwei0314@gmail.com>
# Copyright (C) 2018. All Rights Reserved.
import numpy
from sklearn import metrics
import torch
import torch.nn as nn
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter
import argparse
import math
import os

from data_utils import build_tokenizer, build_embedding_matrix, ABSADataset, ABSAFeaturesDataset

from models.lstm_ca import LSTM_CA
from models.lstm_ca_feat import LSTM_CA_Feat

context_dict = {
    'cadec': 236,
}

features_dict = {
    'cadec': [5257]
}

RESULT_OUTPUT_DIR = "output/results_avg.txt"

class Instructor:
    def __init__(self, opt):
        self.opt = opt

        tokenizer = build_tokenizer(
            fnames=[opt.dataset_file['train'], opt.dataset_file['test']],
            max_seq_len=opt.max_seq_len,
            dat_fname='{0}_tokenizer.dat'.format(opt.dataset))
        embedding_matrix = build_embedding_matrix(
            word2idx=tokenizer.word2idx,
            embed_dim=opt.embed_dim,
            embed_file=opt.embed_file,
            dat_fname='{0}_{1}_embedding_matrix.dat'.format(str(opt.embed_dim), opt.dataset))
        self.model = opt.model_class(embedding_matrix, opt).to(opt.device)

        if opt.model_name == "lstm_ca":
            trainset = ABSADataset(opt.dataset_file['train'], tokenizer)
            testset = ABSADataset(opt.dataset_file['test'], tokenizer)
        else:
            trainset = ABSAFeaturesDataset(opt.dataset_file['train'], tokenizer, opt.dataset_file['train_features'],
                                           opt.vectorizer, opt.transformer, is_train=True)
            testset = ABSAFeaturesDataset(opt.dataset_file['test'], tokenizer, opt.dataset_file['test_features'],
                                          opt.vectorizer, opt.transformer, is_train=False)
        self.train_data_loader = DataLoader(dataset=trainset, batch_size=opt.batch_size, shuffle=True)
        self.test_data_loader = DataLoader(dataset=testset, batch_size=opt.batch_size, shuffle=False)
        print("features length = " + str(opt.features_length))

        if opt.device.type == 'cuda':
            print("cuda memory allocated:", torch.cuda.memory_allocated(device=opt.device.index))
        self._print_args()

    def _print_args(self):
        n_trainable_params, n_nontrainable_params = 0, 0
        for p in self.model.parameters():
            n_params = torch.prod(torch.tensor(p.shape))
            if p.requires_grad:
                n_trainable_params += n_params
            else:
                n_nontrainable_params += n_params
        print('n_trainable_params: {0}, n_nontrainable_params: {1}'.format(n_trainable_params, n_nontrainable_params))
        print('> training arguments:')
        for arg in vars(self.opt):
            print('>>> {0}: {1}'.format(arg, getattr(self.opt, arg)))

    def _reset_params(self):
        for child in self.model.children():
            for p in child.parameters():
                if p.requires_grad:
                    if len(p.shape) > 1:
                        self.opt.initializer(p)
                    else:
                        stdv = 1. / math.sqrt(p.shape[0])
                        torch.nn.init.uniform_(p, a=-stdv, b=stdv)

    def _train(self, criterion, optimizer):
        writer = SummaryWriter(log_dir=self.opt.logdir)
        max_test_acc = 0
        max_f1 = 0
        global_step = 0
        pred = []
        gold = []
        for epoch in range(self.opt.num_epoch):
            print('>' * 100)
            print('epoch: ', epoch)
            n_correct, n_total = 0, 0
            for i_batch, sample_batched in enumerate(self.train_data_loader):
                global_step += 1

                # switch model to training mode, clear gradient accumulators
                self.model.train()
                optimizer.zero_grad()

                inputs = [sample_batched[col].to(self.opt.device) for col in self.opt.inputs_cols]
                outputs = self.model(inputs)
                targets = sample_batched['polarity'].to(self.opt.device)

                loss = criterion(outputs, targets)
                loss.backward()
                optimizer.step()

                if global_step % self.opt.log_step == 0:
                    n_correct += (torch.argmax(outputs, -1) == targets).sum().item()
                    n_total += len(outputs)
                    train_acc = n_correct / n_total

                    # switch model to evaluation mode
                    self.model.eval()
                    test_acc, f1, p, r, p1, r1, f11, p0, r0, f10 = self._evaluate_acc_f1()
                    if test_acc > max_test_acc:
                        max_test_acc = test_acc
                        if not os.path.exists('state_dict'):
                            os.mkdir('state_dict')
                        path = 'state_dict/{0}_{1}_acc{2}'.format(self.opt.model_name, self.opt.dataset, round(test_acc, 4))
                        torch.save(self.model.state_dict(), path)
                        print('>> saved: ' + path)
                    if f1 > max_f1:
                        max_f1 = f1

                    writer.add_scalar('loss', loss, global_step)
                    writer.add_scalar('acc', train_acc, global_step)
                    writer.add_scalar('test_acc', test_acc, global_step)
                    print('loss: {:.4f}, acc: {:.4f}, test_acc: {:.4f}, f1: {:.4f}'.format(loss.item(), train_acc, test_acc, f1))

            if epoch == self.opt.num_epoch -1:
                print("Epoch num = " + str(epoch))
                test_acc, f1, p, r, p1, r1, f11, p0, r0, f10 = self._evaluate_acc_f1()
                precision.append(p)
                recall.append(r)
                f_measure.append(f1)

                precision1.append(p1)
                recall1.append(r1)
                f_measure1.append(f11)

                precision0.append(p0)
                recall0.append(r0)
                f_measure0.append(f10)


        writer.close()
        return max_test_acc, max_f1

    def _evaluate_acc_f1(self):
        n_test_correct, n_test_total = 0, 0
        t_targets_all, t_outputs_all = None, None
        with torch.no_grad():
            for t_batch, t_sample_batched in enumerate(self.test_data_loader):
                t_inputs = [t_sample_batched[col].to(opt.device) for col in self.opt.inputs_cols]
                t_targets = t_sample_batched['polarity'].to(opt.device)
                t_outputs = self.model(t_inputs)

                n_test_correct += (torch.argmax(t_outputs, -1) == t_targets).sum().item()
                n_test_total += len(t_outputs)

                if t_targets_all is None:
                    t_targets_all = t_targets
                    t_outputs_all = t_outputs
                else:
                    t_targets_all = torch.cat((t_targets_all, t_targets), dim=0)
                    t_outputs_all = torch.cat((t_outputs_all, t_outputs), dim=0)

        test_acc = n_test_correct / n_test_total
        print(n_test_total)
        p, r, f1, s = metrics.precision_recall_fscore_support(t_targets_all.cpu(), torch.argmax(t_outputs_all, -1).cpu(), labels=[0, 1], average='macro')
        p1, r1, f11, s1 = metrics.precision_recall_fscore_support(t_targets_all.cpu(), torch.argmax(t_outputs_all, -1).cpu(), labels=[1], average='macro')
        p0, r0, f10, s0 = metrics.precision_recall_fscore_support(t_targets_all.cpu(), torch.argmax(t_outputs_all, -1).cpu(), labels=[0], average='macro')
        return test_acc, f1, p, r, p1, r1, f11, p0, r0, f10

    def run(self):
        # Loss and Optimizer
        criterion = nn.CrossEntropyLoss()
        _params = filter(lambda p: p.requires_grad, self.model.parameters())
        optimizer = self.opt.optimizer(_params, lr=self.opt.learning_rate, weight_decay=self.opt.l2reg)

        self._reset_params()
        max_test_acc, max_f1 = self._train(criterion, optimizer)
        print('max_test_acc: {0}     max_f1: {1}'.format(max_test_acc, max_f1))


if __name__ == '__main__':

    # Hyper Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', default='lstm_ca_feat', type=str)
    parser.add_argument('--dataset', default='cadec')
    parser.add_argument('--optimizer', default='adam', type=str)
    parser.add_argument('--initializer', default='xavier_uniform_', type=str)
    parser.add_argument('--learning_rate', default=0.01, type=float)
    parser.add_argument('--dropout', default=0.1, type=float)
    parser.add_argument('--l2reg', default=0.01, type=float)
    parser.add_argument('--num_epoch', default=15, type=int)
    parser.add_argument('--batch_size', default=32, type=int)
    parser.add_argument('--log_step', default=5, type=int)
    parser.add_argument('--logdir', default='log', type=str)
    parser.add_argument('--embed_dim', default=200, type=int)
    parser.add_argument('--embed_file', default='path/to/word/embedding', type=str)
    parser.add_argument('--hidden_dim', default=300, type=int)
    parser.add_argument('--max_seq_len', default=80, type=int)
    parser.add_argument('--polarities_dim', default=2, type=int)
    parser.add_argument('--hops', default=3, type=int)
    parser.add_argument('--device', default=None, type=str)
    parser.add_argument('--fold_nums', default=1, type=int)
    opt = parser.parse_args()

    model_classes = {
        'lstm_ca': LSTM_CA,
        'lstm_ca_feat': LSTM_CA_Feat,
    }
    input_colses = {
        'lstm_ca': ['text_raw_indices', 'entity_indices'],
        'lstm_ca_feat': ['text_raw_indices', 'entity_indices', 'features']
    }
    initializers = {
        'xavier_uniform_': torch.nn.init.xavier_uniform_,
        'xavier_normal_': torch.nn.init.xavier_normal,
        'orthogonal_': torch.nn.init.orthogonal_,
    }
    optimizers = {
        'adadelta': torch.optim.Adadelta,  # default lr=1.0
        'adagrad': torch.optim.Adagrad,  # default lr=0.01
        'adam': torch.optim.Adam,  # default lr=0.001
        'adamax': torch.optim.Adamax,  # default lr=0.002
        'asgd': torch.optim.ASGD,  # default lr=0.01
        'rmsprop': torch.optim.RMSprop,  # default lr=0.01
        'sgd': torch.optim.SGD,
    }
    opt.model_class = model_classes[opt.model_name]
    opt.inputs_cols = input_colses[opt.model_name]
    opt.initializer = initializers[opt.initializer]
    opt.optimizer = optimizers[opt.optimizer]
    opt.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') \
        if opt.device is None else torch.device(opt.device)
    opt.max_seq_len = context_dict[opt.dataset]
    opt.vectorizer = TfidfVectorizer(ngram_range=(1, 1), max_features=2000)
    opt.transformer = TfidfTransformer()

    precision = []
    recall = []
    f_measure = []

    precision1 = []
    recall1 = []
    f_measure1 = []

    precision0 = []
    recall0 = []
    f_measure0 = []

    for fold_num in range(1, opt.fold_nums + 1):
        opt.features_length = features_dict[opt.dataset][fold_num - 1]
        dataset_files = {
            'cadec': {
                'train': './datasets/cadec/folds/' + str(fold_num) + '/train.txt',
                'test': './datasets/cadec/folds/' + str(fold_num) + '/test.txt',
                'train_features': './datasets/cadec/folds/' + str(fold_num) + '/train_features.txt',
                'test_features': './datasets/cadec/folds/' + str(fold_num) + '/test_features.txt'
            }
        }
        opt.dataset_file = dataset_files[opt.dataset]
        ins = Instructor(opt)
        ins.run()
        out = open("output/results_by_folds.txt", 'a')
        out.write(opt.dataset + " & " + opt.model_name + " & " + str(opt.num_epoch) + " & " +
                  str(precision[-1]) + " & " + str(recall[-1]) + " & " + str(f_measure[-1]) + " & " +
                  str(precision1[-1]) + " & " + str(recall1[-1]) + " & " + str(f_measure1[-1]) + " & " +
                  str(precision0[-1]) + " & " + str(recall0[-1]) + " & " + str(f_measure0[-1]) + '\n')
    out = open(RESULT_OUTPUT_DIR, 'a')
    out.write(opt.dataset + " & " + opt.model_name + " & " + str(opt.num_epoch) + " & " +
              str(numpy.average(precision0)) + " & " + str(numpy.average(recall0)) + " & " + str(numpy.average(f_measure0)) + " & " +
              str(numpy.average(precision1)) + " & " + str(numpy.average(recall1)) + " & " + str(numpy.average(f_measure1)) + " & " +
              str(numpy.average(precision)) + " & " + str(numpy.average(recall)) + " & " + str(numpy.average(f_measure)) + '\n')
    print(len(precision))
    print(len(recall))
    print(len(f_measure))